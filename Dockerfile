FROM centos:7

LABEL Description="cofolla-toolbox"

RUN yum install -y epel-release && yum -y update && yum install -y \
    vim wget telnet bind-utils tcpdump bash-completion bash-completion-extras \
    krb5-workstation \
    crudini gcc gdisk git jq libffi-devel libxml2-devel libxslt-devel make \
    mariadb mariadb-devel openssh-clients openssl-devel python-devel \
    && yum clean all \
    && rm -rf /var/cache/yum

RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python get-pip.py \
    && rm get-pip.py

RUN pip --no-cache-dir install --upgrade virtualenv \
    && virtualenv --system-site-packages /opt/ansible

ENV PATH /opt/ansible/bin:$PATH

RUN pip --no-cache-dir install --upgrade ansible==2.7.1 "cmd2<0.9.0" MySQL-python \
    os-client-config==1.31.2 pbr==4.0.0 pymongo python-openstackclient==3.16.1 python-heatclient==1.16.1 pytz pyudev shade==1.30.0 \
    && mkdir -p /etc/ansible /usr/share/ansible \
    && echo 'localhost ansible_connection=local ansible_python_interpreter=/opt/ansible/bin/python' > /etc/ansible/hosts \
    && sed -i 's|  "identity_api_version": "2.0",|  "identity_api_version": "3",|' /opt/ansible/lib/python2.7/site-packages/os_client_config/defaults.json \
    && sed -i 's|  "volume_api_version": "2",|  "volume_api_version": "3",|' /opt/ansible/lib/python2.7/site-packages/os_client_config/defaults.json \
    && /opt/ansible/bin/openstack complete | tee /etc/bash_completion.d/osc.bash_completion > /dev/null

ENV ANSIBLE_LIBRARY /usr/share/ansible:$ANSIBLE_LIBRARY

RUN useradd -ms /bin/bash cofolla
USER cofolla
WORKDIR /home/cofolla/cofolla_repo
